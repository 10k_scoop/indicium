import React from "react";
import Discover from "../../pages/Discover";

const DiscoverRoute = () => {
  return <Discover />;
};

export default DiscoverRoute;
