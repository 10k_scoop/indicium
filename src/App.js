import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import "./App.css";
import DiscoverRoute from "./routes/landing-pages/DiscoverRoute";
import DiscoverRouteSingle from "./routes/landing-pages/DiscoverSingleRoute";
import HomeRoute from "./routes/landing-pages/HomeRoute";
import ProfileRoute from "./routes/landing-pages/ProfileRoute";
import SignupRoute from "./routes/landing-pages/SignupRoute";
//import * as firebase from "firebase"

function App() {


  return (
    <Router>
      <Switch>
        <Route path="/discover">
          <DiscoverRoute />
        </Route>
        <Route path="/discoverSingle">
          <DiscoverRouteSingle />
        </Route>
        <Route path="/profile">
          <ProfileRoute />
        </Route>
        <Route path="/signup">
          <SignupRoute />
        </Route>
        <Route path="/">
          <HomeRoute />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
