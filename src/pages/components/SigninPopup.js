import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../styles/Home.css";
import "../../styles/Responsive.css";
import logo from "../../assets/images/logo.png"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faEye, faTimes } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

const SigninPopup = (props) => {
    const [showPass, setShowPass] = useState(false);
    const [rememberMe, setRememberMe] = useState(false);

    return (
        <div className="popupBodyWrapper">
            <div className="popupBodyWrapperLayer"></div>
            <div className="container-fluid popupBodyContainerLinear">
                <div className="popupBodyContainerLayer"></div>

                <div className="popupBodyContainer">

                    <div className="row popupHeaderRow">
                        <div className="col-sm-6">
                            <img src={logo} className="popupLogo" />
                        </div>
                        <div className="col-sm-6 popUpCloseBtn" onClick={() => props.setShowPopup(!props.showPopup)}>
                            <FontAwesomeIcon icon={faTimes} color="black" />
                        </div>
                    </div>
                    {/* Popup header ends here */}


                    {/* Popup Form Starts here */}
                    <div className="formWrapper">

                        <form>
                            {/* Email Field */}
                            <div className="inputWrapper">
                                <div className="inputLabel">Email address</div>
                                <div className="inputFieldWrapper">
                                    <div className="inputFieldWrapperLayer"></div>
                                    <input type="email" placeholder="your@email.com" className="inputField" />
                                </div>
                            </div>
                            {/* Password Field */}
                            <div className="inputWrapper">
                                <div className="inputLabel">Password</div>
                                <div className="inputFieldWrapper">
                                    <div className="inputFieldWrapperLayer"></div>
                                    <input autofocus="false" type={showPass ? "text" : "password"} placeholder="********" className="inputField" />
                                    <FontAwesomeIcon onClick={() => setShowPass(!showPass)} icon={faEye} color={showPass ? "white" : "#8FA3AD"} />
                                </div>
                            </div>
                            {/* Remember me and Forget Password */}
                            <div className="row">
                                <div className="col-sm-6 rememberMeText">
                                    <div className="rememberMeToggleBtn" onClick={() => setRememberMe(!rememberMe)}>
                                        {rememberMe && <FontAwesomeIcon icon={faCheck} color="black" style={{ fontSize: 11 }} />}
                                    </div>
                                    Remember me
                                </div>
                                <div className="col-sm-6 forgetPassText"> Forgot your password?</div>
                            </div>

                            {/* Signin Button */}
                            <Link to="/profile">
                                <div className="signInBtnWrapper">
                                    Sign in
                                </div>
                            </Link>
                        </form>
                    </div>
                    {/* Popup Form Ends here */}
                </div>
            </div>
        </div>
    );
};

export default SigninPopup;
