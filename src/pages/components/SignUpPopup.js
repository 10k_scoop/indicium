import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../../styles/Home.css";
import "../../styles/Responsive.css";
import logo from "../../assets/images/logo.png"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

const SignUpPopup = (props) => {

    return (
        <div className="popupBodyWrapper">
            <div className="popupBodyWrapperLayer"></div>
            <div className="container-fluid popupBodyContainerLinear2">
                <div className="popupBodyContainerLayer"></div>

                <div className="popupBodyContainer">

                    <div className="row popupHeaderRow">
                        <div className="col-sm-11 signupPopupTitle">
                            Join to the Indicium
                        </div>
                        <div className="col-sm-1 popUpCloseBtn" onClick={() => props.setShowSignupPopup(!props.showSignupPopup)}>
                            <FontAwesomeIcon icon={faTimes} color="black" />
                        </div>
                    </div>
                    {/* Popup header ends here */}


                    {/* Popup Content Starts here */}
                    <div className="ContentWrapper">
                        <div className="ContentWrapperText1">
                            We have noticed you don’t have a Neftly account yet
                        </div>
                        <Link to="/signup">
                            <div className="signInBtnWrapper">
                                Register My Account
                            </div>
                        </Link>
                        <div className="ContentWrapperText2">
                            Bring your profile to the crowd and start receiving notification
                            <br />
                            about your activities and NFT updates
                        </div>
                    </div>
                    {/* Popup Content Ends here */}
                </div>
            </div>
        </div>
    );
};

export default SignUpPopup;
